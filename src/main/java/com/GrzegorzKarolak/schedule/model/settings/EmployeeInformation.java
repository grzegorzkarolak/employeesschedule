package com.GrzegorzKarolak.schedule.model.settings;

import com.GrzegorzKarolak.schedule.model.helpers.LocalDateTimePeriod;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Set;

public class EmployeeInformation {

    private Double partOfWorkTime;
    private Set<LocalDate> holidays;
    private Set<LocalDate> otherOffs;
    private double normalNoHoursPerDay;
    private Set<DayOfWeek> cannotWorkDaysOfWeek;

    public EmployeeInformation() {
    }

    public EmployeeInformation(Double partOfWorkTime, Set<LocalDate> holidays, Set<LocalDate> otherOffs, double normalNoHoursPerDay, Set<DayOfWeek> cannotWorkDaysOfWeek) {
        this.partOfWorkTime = partOfWorkTime;
        this.holidays = holidays;
        this.otherOffs = otherOffs;
        this.normalNoHoursPerDay = normalNoHoursPerDay;
        this.cannotWorkDaysOfWeek = cannotWorkDaysOfWeek;
    }

    public double getPartOfWorkTime() {
        return partOfWorkTime;
    }

    public void setPartOfWorkTime(Double partOfWorkTime) {
        this.partOfWorkTime = partOfWorkTime;
    }

    public Set<LocalDate> getHolidays() {
        return holidays;
    }

    public void setHolidays(Set<LocalDate> holidays) {
        this.holidays = holidays;
    }

    public Set<LocalDate> getOtherOffs() {
        return otherOffs;
    }

    public void setOtherOffs(Set<LocalDate> otherOffs) {
        this.otherOffs = otherOffs;
    }

    public double getNormalNoHoursPerDay() {
        return normalNoHoursPerDay;
    }

    public void setNormalNoHoursPerDay(double normalNoHoursPerDay) {
        this.normalNoHoursPerDay = normalNoHoursPerDay;
    }

    public Set<DayOfWeek> getCannotWorkDaysOfWeek() {
        return cannotWorkDaysOfWeek;
    }

    public void setCannotWorkDaysOfWeek(Set<DayOfWeek> cannotWorkDaysOfWeek) {
        this.cannotWorkDaysOfWeek = cannotWorkDaysOfWeek;
    }
}
