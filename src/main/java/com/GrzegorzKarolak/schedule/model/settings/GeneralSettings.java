package com.GrzegorzKarolak.schedule.model.settings;

import java.time.YearMonth;
import java.util.Map;

public class GeneralSettings {

    private static Map<YearMonth, Integer> workingHoursPerMonth;
    private static int maxWorkDaysInRow;
    private static int minHoursOffInRow;
    private static double fullTimeWorkingHoursPerDay;

    public GeneralSettings() {
    }

    public static Map<YearMonth, Integer> getWorkingHoursPerMonth() {
        return workingHoursPerMonth;
    }

    public static void setWorkingHoursPerMonth(Map<YearMonth, Integer> workingHoursPerMonth) {
        GeneralSettings.workingHoursPerMonth = workingHoursPerMonth;
    }

    public static int getMaxWorkDaysInRow() {
        return maxWorkDaysInRow;
    }

    public static void setMaxWorkDaysInRow(int maxWorkDaysInRow) {
        GeneralSettings.maxWorkDaysInRow = maxWorkDaysInRow;
    }

    public static int getMinHoursOffInRow() {
        return minHoursOffInRow;
    }

    public static void setMinHoursOffInRow(int minHoursOffInRow) {
        GeneralSettings.minHoursOffInRow = minHoursOffInRow;
    }

    public static double getFullTimeWorkingHoursPerDay() {
        return fullTimeWorkingHoursPerDay;
    }

    public static void setFullTimeWorkingHoursPerDay(double fullTimeWorkingHoursPerDay) {
        GeneralSettings.fullTimeWorkingHoursPerDay = fullTimeWorkingHoursPerDay;
    }
}
