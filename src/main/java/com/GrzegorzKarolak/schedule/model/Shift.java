package com.GrzegorzKarolak.schedule.model;

import com.GrzegorzKarolak.schedule.model.helpers.LocalTimePeriod;

import java.time.DayOfWeek;
import java.util.Set;

public class Shift {

    private LocalTimePeriod shift;
    private double duration;
    private Position position;
    private Set<DayOfWeek> daysOfWeek;

    public Shift() {
    }

    public Shift(LocalTimePeriod shift, double duration, Position position, Set<DayOfWeek> daysOfWeek) {
        this.shift = shift;
        this.duration = duration;
        this.position = position;
        this.daysOfWeek = daysOfWeek;
    }

    public LocalTimePeriod getShift() {
        return shift;
    }

    public void setShift(LocalTimePeriod shift) {
        this.shift = shift;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Set<DayOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Set<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }
}
