package com.GrzegorzKarolak.schedule.model.schedules;

import com.GrzegorzKarolak.schedule.model.Employee;
import com.GrzegorzKarolak.schedule.model.Shift;

import java.time.YearMonth;
import java.util.List;
import java.util.Map;

public class Schedule {

    private YearMonth month;
    private List<SingleSchedule> singleSchedules;
    private double hoursBalance;

    public Schedule() {
    }

    public Schedule(YearMonth month, List<SingleSchedule> singleSchedules, double hoursBalance) {
        this.month = month;
        this.singleSchedules = singleSchedules;
        this.hoursBalance = hoursBalance;
    }

    public YearMonth getMonth() {
        return month;
    }

    public void setMonth(YearMonth month) {
        this.month = month;
    }

    public List<SingleSchedule> getSingleSchedules() {
        return singleSchedules;
    }

    public void setSingleSchedules(List<SingleSchedule> singleSchedules) {
        this.singleSchedules = singleSchedules;
    }

    public double getHoursBalance() {
        return hoursBalance;
    }

    public void setHoursBalance(double hoursBalance) {
        this.hoursBalance = hoursBalance;
    }
}
