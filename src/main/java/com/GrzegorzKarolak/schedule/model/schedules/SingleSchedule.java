package com.GrzegorzKarolak.schedule.model.schedules;

import com.GrzegorzKarolak.schedule.model.Employee;
import com.GrzegorzKarolak.schedule.model.helpers.LocalDateTimePeriod;

import java.util.List;
import java.util.Map;

public class SingleSchedule {

    private Employee employee;
    private List<LocalDateTimePeriod> workingDays;

    public SingleSchedule() {
    }

    public SingleSchedule(Employee employee, List<LocalDateTimePeriod> workingDays) {
        this.employee = employee;
        this.workingDays = workingDays;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<LocalDateTimePeriod> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<LocalDateTimePeriod> workingDays) {
        this.workingDays = workingDays;
    }
}
