package com.GrzegorzKarolak.schedule.model;

import com.GrzegorzKarolak.schedule.model.settings.EmployeeInformation;

public class Employee {

    private int id;
    private String name;
    private String lastName;
    private Position position;
    private EmployeeInformation employeeInformation;

    public Employee() {
    }

    public Employee(String name, String lastName, Position position, EmployeeInformation employeeInformation) {
        this.name = name;
        this.lastName = lastName;
        this.position = position;
        this.employeeInformation = employeeInformation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public EmployeeInformation getEmployeeInformation() {
        return employeeInformation;
    }

    public void setEmployeeInformation(EmployeeInformation employeeInformation) {
        this.employeeInformation = employeeInformation;
    }
}
