package com.GrzegorzKarolak.schedule.model.helpers;

import java.time.LocalTime;

public class LocalTimePeriod {

    private LocalTime start;
    private LocalTime finish;

    public LocalTimePeriod() {
    }

    public LocalTimePeriod(LocalTime start, LocalTime finish) {
        this.start = start;
        this.finish = finish;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getFinish() {
        return finish;
    }

    public void setFinish(LocalTime finish) {
        this.finish = finish;
    }
}
