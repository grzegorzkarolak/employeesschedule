package com.GrzegorzKarolak.schedule.model.helpers;

import java.time.LocalDateTime;

public class LocalDateTimePeriod {

    private LocalDateTime start;
    private LocalDateTime finish;

    public LocalDateTimePeriod() {
    }

    public LocalDateTimePeriod(LocalDateTime start, LocalDateTime finish) {
        this.start = start;
        this.finish = finish;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public void setFinish(LocalDateTime finish) {
        this.finish = finish;
    }
}
