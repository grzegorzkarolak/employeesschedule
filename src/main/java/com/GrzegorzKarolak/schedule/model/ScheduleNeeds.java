package com.GrzegorzKarolak.schedule.model;

import java.time.Month;
import java.time.YearMonth;
import java.util.List;
import java.util.Map;

public class ScheduleNeeds {

    private Map<Shift, Double> employeeRequest;
    private List<Employee> availableEmployees;
    private YearMonth month;

    public ScheduleNeeds() {
    }

    public ScheduleNeeds(Map<Shift, Double> employeeRequest, List<Employee> availableEmployees, YearMonth month) {
        this.employeeRequest = employeeRequest;
        this.availableEmployees = availableEmployees;
        this.month = month;
    }

    public Map<Shift, Double> getEmployeeRequest() {
        return employeeRequest;
    }

    public void setEmployeeRequest(Map<Shift, Double> employeeRequest) {
        this.employeeRequest = employeeRequest;
    }

    public List<Employee> getAvailableEmployees() {
        return availableEmployees;
    }

    public void setAvailableEmployees(List<Employee> availableEmployees) {
        this.availableEmployees = availableEmployees;
    }

    public YearMonth getMonth() {
        return month;
    }

    public void setMonth(YearMonth month) {
        this.month = month;
    }
}
