package com.GrzegorzKarolak.schedule.facade;

import com.GrzegorzKarolak.schedule.model.Employee;
import com.GrzegorzKarolak.schedule.model.Position;
import com.GrzegorzKarolak.schedule.model.ScheduleNeeds;
import com.GrzegorzKarolak.schedule.model.Shift;
import com.GrzegorzKarolak.schedule.model.schedules.Schedule;
import com.GrzegorzKarolak.schedule.model.settings.GeneralSettings;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CreateScheduleFacade {

    private ScheduleNeeds scheduleNeeds;

    public CreateScheduleFacade(ScheduleNeeds scheduleNeeds) {
        this.scheduleNeeds = scheduleNeeds;
    }

    public Schedule createSchedule() {
        YearMonth month = scheduleNeeds.getMonth();



        return null;
    }

    public Map<Position, Double> getPositionsHoursBalance() {
        Map<Position, Double> requiredPositionsHours = new HashMap<>();
        for (Map.Entry<Shift, Double> need : scheduleNeeds.getEmployeeRequest().entrySet()) {
            Position position = need.getKey().getPosition();
            Double hoursForPosition = null;
            hoursForPosition = requiredPositionsHours.containsKey(position) ? requiredPositionsHours.get(position) : Double.valueOf(0);
            hoursForPosition += need.getValue();
            requiredPositionsHours.put(position, hoursForPosition);
        }
        Map<Position, Double> availablePositionsHours = new HashMap<>();
        for (Employee employee : scheduleNeeds.getAvailableEmployees()) {
            Position position = employee.getPosition();
            Double hoursForPosition = null;
            hoursForPosition = availablePositionsHours.containsKey(position) ? availablePositionsHours.get(position) : Double.valueOf(0);
            double holidaysHours = 0;
            Set<LocalDate> currentMonthHolidayDays = employee.getEmployeeInformation().getHolidays()
                    .stream()
                    .filter(h -> scheduleNeeds.getMonth().getMonth().equals(h.getMonth()))
                    .collect(Collectors.toSet());
            double holidayDaysNumber = (double) currentMonthHolidayDays.size();
            holidaysHours += holidayDaysNumber * (employee.getEmployeeInformation().getPartOfWorkTime() * GeneralSettings.getFullTimeWorkingHoursPerDay());
            double numbersOfHoursPerMonth = employee.getEmployeeInformation().getPartOfWorkTime() * GeneralSettings.getWorkingHoursPerMonth().get(scheduleNeeds.getMonth());
            hoursForPosition += numbersOfHoursPerMonth - holidaysHours;
            availablePositionsHours.put(position, hoursForPosition);
        }
        Map<Position, Double> result = new HashMap<>();
        requiredPositionsHours.forEach((k, v) -> result.put(k, v - availablePositionsHours.get(k)));
        return result;
    }

}
