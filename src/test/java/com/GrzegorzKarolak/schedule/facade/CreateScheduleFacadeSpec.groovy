package com.GrzegorzKarolak.schedule.facade

import com.GrzegorzKarolak.schedule.builders.EmployeeBuilder
import com.GrzegorzKarolak.schedule.builders.ShiftBuilder
import com.GrzegorzKarolak.schedule.model.Employee
import com.GrzegorzKarolak.schedule.model.Position
import com.GrzegorzKarolak.schedule.model.ScheduleNeeds
import com.GrzegorzKarolak.schedule.model.Shift
import com.GrzegorzKarolak.schedule.model.data.DataLists
import javafx.geometry.Pos
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalTime

class CreateScheduleFacadeSpec extends Specification {

    @Shared
    Employee employee1 = EmployeeBuilder.aEmployee()
            .withPosition(position = Mock())
            .withEmployeeInformation((employeeInformation = Mock()))
    @Shared
    Employee employee2 = EmployeeBuilder.aEmployee()
            .withPosition(position = Mock())
            .withEmployeeInformation((employeeInformation = Mock()))
    @Shared
    Employee employee3 = EmployeeBuilder.aEmployee()
            .withPosition(position = Mock())
            .withEmployeeInformation((employeeInformation = Mock()))

    @Shared
    Shift shift1 = ShiftBuilder.aShiftBuilder(new LocalTime(LocalTime.of(6,0), LocalTime.of(14, 0)))
    @Shared
    Shift shift2 = ShiftBuilder.aShiftBuilder(new LocalTime(LocalTime.of(14,0), LocalTime.of(22, 0)))
    @Shared
    Shift shift3 = ShiftBuilder.aShiftBuilder(new LocalTime(LocalTime.of(22,0), LocalTime.of(6, 0)))


    def "should calculate hours balance for given month and positions"() {

        given:"3 employees"

        ScheduleNeeds scheduleNeeds = new ScheduleNeeds()



        when: " "
        CreateScheduleFacade facade = new CreateScheduleFacade(scheduleNeeds)
        def result = facade.getPositionsHoursBalance()








    }



}
