package com.GrzegorzKarolak.schedule.builders

import com.GrzegorzKarolak.schedule.model.Employee
import com.GrzegorzKarolak.schedule.model.ScheduleNeeds
import com.GrzegorzKarolak.schedule.model.Shift
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

import java.time.YearMonth

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class ScheduleNeedsBuilder {

    Map<Shift, Double> employeeRequest;
    List<Employee> availableEmployees;
    YearMonth month;

    static ScheduleNeedsBuilder aScheduleNeedsBuilder() {
        return new ScheduleNeedsBuilder();
    }

    ScheduleNeeds build() {
        return new ScheduleNeeds(employeeRequest, availableEmployees, month)
    }
}
