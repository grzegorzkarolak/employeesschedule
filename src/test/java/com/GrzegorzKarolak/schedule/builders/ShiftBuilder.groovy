package com.GrzegorzKarolak.schedule.builders

import com.GrzegorzKarolak.schedule.model.Position
import com.GrzegorzKarolak.schedule.model.Shift
import com.GrzegorzKarolak.schedule.model.helpers.LocalTimePeriod
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

import java.time.DayOfWeek

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class ShiftBuilder {

    LocalTimePeriod shift;
    double duration;
    Position position;
    Set<DayOfWeek> daysOfWeek;

    static ShiftBuilder aShiftBuilder() {
        return new ShiftBuilder();
    }

    Shift build() {
        return new Shift(shift, duration, position, daysOfWeek)
    }
}
