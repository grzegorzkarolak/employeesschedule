package com.GrzegorzKarolak.schedule.builders

import com.GrzegorzKarolak.schedule.model.Employee
import com.GrzegorzKarolak.schedule.model.Position
import com.GrzegorzKarolak.schedule.model.settings.EmployeeInformation
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class EmployeeBuilder {

    static Random generator = new Random();

    String name = "SampleName" + generator.nextInt(50);
    String lastName = "SampleLastName" + generator.nextInt(50);
    Position position;
    EmployeeInformation employeeInformation;

    static EmployeeBuilder aEmployee() {
        return new EmployeeBuilder();
    }

    Employee build(){
        return new Employee(name, lastName, position, employeeInformation)
    }
}