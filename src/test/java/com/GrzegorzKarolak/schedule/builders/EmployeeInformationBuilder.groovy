package com.GrzegorzKarolak.schedule.builders

import com.GrzegorzKarolak.schedule.model.Employee
import com.GrzegorzKarolak.schedule.model.settings.EmployeeInformation
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

import java.time.DayOfWeek
import java.time.LocalDate

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class EmployeeInformationBuilder {

    Double partOfWorkTime;
    Set<LocalDate> holidays;
    Set<LocalDate> otherOffs;
    double normalNoHoursPerDay;
    Set<DayOfWeek> cannotWorkDaysOfWeek;

    static EmployeeInformationBuilder aEmployeeInformationBuilder() {
        return new EmployeeInformationBuilder();
    }

    EmployeeInformation build(){
        return new EmployeeInformation(partOfWorkTime, holidays, otherOffs, normalNoHoursPerDay, cannotWorkDaysOfWeek)
    }
}
